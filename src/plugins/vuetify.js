import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'mdi'
  },
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#FF5722',
        secondary: '#FF8A50',
        accent: '#FF5722',
        error: '#F44336',
        info: '#03A9F4',
        success: '#00C853',
        warning: '#FFEA00'
      }
    }
  }
})
