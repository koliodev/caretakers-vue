import axios from 'axios'
import moment from 'moment'
import _ from 'lodash'

export default {
  state: {
    suggestedCaretakers: null,
    caretakerSearchParams: null,
    caretaker: null,
    cellendarInfo: null
  },
  actions: {

    // Намира подходящи гледачи за лице
    async findCaretakers ({ state, rootState }, { individual, times, weekly, proximity, skills }) {
      let url = `/caretaker/find/`
      var data
      try {
        ({ data } = await axios.get(url, {
          params: {
            _token: rootState.auth.token,
            proximity,
            individual_id: individual,
            skills
          }
        }))
      } catch ({ response }) {
        throw response
      };

      state.suggestedCaretakers = data
      state.caretakerSearchParams = { individual, times, weekly, proximity, skills }
    },

    // връща данните за каретакер с дадено id
    async getCaretaker ({ state, rootState }, { id, setState }) {
      if (typeof setState === 'undefined') {
        setState = true
      }

      let url = `/caretaker/get/` + id
      var data
      try {
        ({ data } = await axios.get(url, {
          params: {
            _token: rootState.auth.token
          }
        }))
      } catch ({ response }) {
        throw response
      };

      if (setState) {
        state.caretaker = data
      }
      return data
    },

    // изпраща заявка към даден caretaker за дадено лице
    async requestCaretaker ({ state }, { individual, caretaker, message, weekly, times }) {
      // оправя чаовете
      var timeToH = time => {
        let t = time.split(':')
        let hs = Number(t[0])
        let ms = Number(t[1])
        return moment.duration(hs, 'hours').add(ms, 'minutes').asHours().toFixed(2)
      }

      var newTimes = {}
      _.each(times, (v, k) => {
        if (!weekly) {
          k += 'T13:23:50.504642Z'
        }
        newTimes[k] = {
          end: Number(timeToH(v.end)),
          start: Number(timeToH(v.start))
        }
      })
      times = JSON.stringify(newTimes)

      var data
      try {
        ({ data } = await axios.post('/hire', {
          individual_id: individual,
          caretaker_id: caretaker,
          message,
          weekly,
          times
        }
        ))
      } catch ({ response }) {
        throw response
      };
      return data
    },

    // променя данните свързани с гледачеството на дадения потребител
    async updateCaretaker ({ rootState }, caretaker) {
      try {
        await axios.post('/caretaker/update', caretaker)
      } catch ({ response }) {
        throw response
      };

      rootState.user.caretaker = caretaker
    },

    // Взима данните за календара
    async getCallendarInfo ({ rootState, state }) {
      var data
      try {
        ({ data } = await axios.get('/calendar', {
          params: {
            _token: rootState.auth.token
          }
        }))
        state.cellendarInfo = data
      } catch ({ response }) {
        throw response
      };
    }

  },
  mutations: {
    setCaretaker (state, caretaker) {
      state.caretaker = caretaker
    },
    clearSearchParams (state) {
      state.caretakerSearchParams = null
    }
  }
}
