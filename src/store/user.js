/* eslint-disable camelcase */
import axios from 'axios'
export default {
  state: {
  },
  actions: {
    // променя данните свързани с гледачеството на дадения потребител
    async updateUser ({ rootState }, {
      email, password, password_confirmation, old_password, first_name, second_name, location, city
    }) {
      console.log('/user/update')
      try {
        await axios.post('/user/update', { email, password, password_confirmation, old_password, first_name, second_name, location, city })
      } catch ({ response }) {
        throw response
      };

      rootState.user.email = email
      rootState.first_name = first_name
      rootState.second_name = second_name
    }
  },
  mutations: {
  }
}
