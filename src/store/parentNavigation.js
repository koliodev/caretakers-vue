export default {
  state: {
    parentRoot: null,
    pageTitle: null
  },
  actions: {

  },
  mutations: {
    setParentRoot (state, root) {
      state.parentRoot = root
    },
    clearParentRoot (state) {
      state.parentRoot = null
    },
    setPageTitle (state, pt) {
      state.pageTitle = pt
    },
    setPageTitleIcon (state, icon) {
      state.pageTitle['icon'] = icon
    },
    clearPageTitle (state) {
      state.pageTitle = null
    }
  }
}
