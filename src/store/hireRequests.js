import axios from 'axios'
import _ from 'lodash'
import globals from '@/globals.js'

var defAvatar = globals.defIndividualAvatar

export default {
  state: {
    requests: [],
    request: null
  },
  actions: {
    // Всички request-и към/на потребителя
    async getHireRequests ({ state, rootState }) {
      try {
        var data
        ({ data } = await axios.get('/hire/requests/all', { params: {
          _token: rootState.auth.token
        } }))

        state.requests = _.map(data, hr => {
          hr.datetime = JSON.parse(hr.datetime)
          hr.user = {}
          if (!hr.individual.picture) {
            hr.individual.picture = defAvatar
          }
          return hr
        })
      } catch ({ response }) {
        throw response
      };
    },
    // Данни за request с id
    async getHireRequest ({ state, rootState }, id) {
      try {
        var data
        ({ data } = await axios.get('/hire/request/' + id, { params: {
          _token: rootState.auth.token
        } }))
        data.datetime = JSON.parse(data.datetime)
        if (!data.individual.picture) {
          data.individual.picture = defAvatar
        }

        state.request = data
        return data
      } catch ({ response }) {
        throw response
      };
    },

    // приема / отхвърля заявка
    async requestChangeStatus ({ state }, { id, status }) {
      let url = `/hire/request/${id}/change/status/${status}`
      try {
        await axios.post(url, {})
      } catch ({ response }) {
        throw response
      };

      if (state.request.id === id) state.request.status = status

      state.requests = _.map(state.requests, r => {
        if (r.id !== id) return r
        r.status = status
        return r
      })
    }
  },
  mutations: {
    setRequest (state, req) {
      state.request = req
    }
  }
}
