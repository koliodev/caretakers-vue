import globals from '@/globals.js'
import Chatkit from '@pusher/chatkit-client'
import axios from 'axios'
import store from '../store.js'
import _ from 'lodash'

export default {
  state: {
    chatkitChatManager: null,
    messages: null,
    mediaCalling: false
  },
  actions: {

    // Изпраща съобщения
    async sendMessage (_, { message, room }) {
      try {
        await axios.post('/send/message', {
          room,
          message
        })
      } catch ({ response }) {
        throw response
      };
    },

    // Връща всички съобщения за дадена стая
    async  getMessages ({ state, rootState }, { room, skip, get }) {
      try {
        var data
        ({ data } = await axios.get('/messages/' + room, {
          params:
          {
            skip: skip,
            get: get,
            _token: rootState.auth.token
          }
        }))

        state.messages = _.reverse(_.map(data, m => {
          return {
            text: m.message,
            left: m.sender !== rootState.user.id,
            id: m.id,
            read: m.read
          }
        }))

        return
      } catch ({ response }) {
        throw response
      };
    },

    // премества курсора за "прочетено"
    async setReadCursor ({ state }, { id }) {
      try {
        id = String(id)
        await axios.post('/message/set/cursor', { id })
      } catch ({ response }) {
        throw response
      };
    },

    // прави/приема/отказва обаждане
    async mediaCall ({ state }, { hireRequest, action }) {
      try {
        let url = `/call/${action}/${hireRequest}`
        await axios.post(url, { })
      } catch ({ response }) {
        throw response
      };
    }
  },
  mutations: {
    setChatManager (state, userId) {
      state.chatkitChatManager = new Chatkit.ChatManager({
        instanceLocator: globals.chatkitInstance,
        userId: userId,
        tokenProvider: new Chatkit.TokenProvider({
          url: globals.apiURL + '/auth/chatter',
          queryParams: {
            '_token': store.getters.getJWT
          }
        })
      })
    },

    setMediaCalling (state, isCalling) {
      state.mediaCalling = isCalling
    }
  }
}
