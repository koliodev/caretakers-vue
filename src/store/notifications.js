import globals from '@/globals.js'
import Pusher from 'pusher-js'
import store from '../store.js'
import axios from 'axios'
import _ from 'lodash'

export default {
  state: {
    pusherClient: null,
    pusherChannel: null,
    notifications: []
  },
  actions: {
    // взима известията от базата
    async getNotifications ({ state, rootState }, { skip, get }) {
      try {
        var data
        ({ data } = await axios.get('/notifications', {
          params:
          {
            skip: skip,
            get: get,
            _token: rootState.auth.token
          }
        }))

        // За да избегне дублиране
        var newData = _.filter(data, d => {
          return !_.find(state.notifications, n => {
            return n.id === d.id
          })
        })

        state.notifications = _.concat(state.notifications, _.map(newData, n => {
          // Опрая проблема с time zone-a

          n.created_at = n.created_at.substring(0, 19)

          return n
        }))
      } catch ({ response }) {
        throw response
      };
    },

    // Задава статус прочетено на някое известие
    async notificationSetRead ({ state }, { id }) {
      try {
        // Заявка към api-то
        await axios.post(`/notification/${id}/read`, { })
      } catch ({ response }) {
        throw response
      };

      // Променя и масива с известията
      state.notifications = _.map(state.notifications, n => {
        if (n.id === id) {
          n.read = true
        }
        return n
      })
    },

    async notificationDelete ({ state }, { id }) {
      try {
        // Заявка към api-то
        await axios.post(`/notification/${id}/remove`, { })
      } catch ({ response }) {
        throw response
      };

      // Променя и масива с известията
      state.notifications = _.filter(state.notifications, n => {
        return n.id !== id
      })
    }

  },
  mutations: {
    // Настройва pusher клиента, отгоарящ за получаването на данни от каналите
    setPusherClient (state) {
      var p = new Pusher(globals.pusher.appId, {
        cluster: globals.pusher.cluster,
        forceTLS: globals.pusher.forceTLS,
        authEndpoint: `${globals.pusher.authUrl}?_token=${store.getters.getJWT}`
      })
      state.pusherClient = p
    },

    // Задава pusher-channel-a за съответния user
    setPusherChannel (state, channel) {
      state.pusherChannel = channel
    },

    // Добавя известие към масива със всички известия
    addNotification (state, notification) {
      // Проверява, дали има notification със същото id
      if (_.find(state.notifications, n => {
        return n.id === notification.id
      })) {
        return
      }

      state.notifications.unshift(notification)
    },

    // Изчиства мастива с известията
    clearNotifications (state) {
      state.notifications = []
    }
  }
}
