import moment from 'moment'
import axios from 'axios'
import Cookie from 'js-cookie'
import _ from 'lodash'
import globals from '@/globals.js'

var defAvatar = globals.defIndividualAvatar

export default {
  state: {
    token: ''
  },
  mutations: {
    setToken (state, token) {
      state.token = token
    }
  },
  actions: {

    // Взима данните на логнат потребител (валиден JWT)
    async getUser ({ state, rootState }) {
      state.token = Cookie.get('jwt_auth_token')

      try {
        var data
        ({ data } = await axios.get('/user', { params: { _token: state.token } }))

        if (data.individuals) {
          data.individuals = _.map(data.individuals, e => {
            if (!e.picture) {
              e.picture = defAvatar
            }
            return e
          })
        }

        rootState.user = data
        return data
      } catch ({ response }) {
        rootState.user = {}
        throw response
      };
    },

    // Log-вa (или регистрира потребител)
    async authUser ({ dispatch, state }, { user, signUp, caretaker }) {
      let url = '/auth'
      if (signUp) { url = '/user/register' }
      if (caretaker) { url = '/caretaker/register' }

      // Прави опит за sign up и ако всичко е 6 запазва JWТ-на в Cookie
      try {
        var data
        ({ data } = await axios.post(url, user))

        if (signUp) return

        let expires = moment.unix(data.expires_at)
        expires.toUTCString = expires.toString

        Cookie.set('jwt_auth_token', data.token, { expires: new Date(expires * 1000) })
        state.token = data.token
      } catch ({ response }) {
        throw response
      }

      // Взима данните на потребителя
      try {
        dispatch('getUser')
      } catch (err) {
        throw err
      }
    },

    logoutUser ({ state, rootState, commit }) {
      state.token = ''
      rootState.user = ''
      Cookie.remove('jwt_auth_token')

      commit('showLoader')
      _.delay(() => { commit('hideLoader') }, 500)
    }
  },
  getters: {
    getJWT (state) {
      return state.token
    }
  }
}
