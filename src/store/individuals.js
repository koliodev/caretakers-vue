import axios from 'axios'
import _ from 'lodash'
import globals from '@/globals.js'

var defAvatar = globals.defIndividualAvatar

export default {
  state: {
    individual: {}
  },
  actions: {

    // Добавя ново лице
    async newIndividual ({ dispatch }, { individual }) {
      try {
        await axios.post('/individuals/add', individual)
        await dispatch('getIndividuals')
      } catch ({ response }) {
        throw response
      };
    },

    // Редактира лице
    async editIndividual ({ dispatch }, { individual }) {
      try {
        await axios.post('/individual/edit/' + individual.id, individual)
        await dispatch('getIndividuals')
      } catch ({ response }) {
        throw response
      };
    },

    // Премахва лице
    async deleteIndividual ({ dispatch }, id) {
      try {
        await axios.post('/individual/remove/' + id, { id })
        await dispatch('getIndividuals')
      } catch ({ response }) {
        throw response
      };
    },

    // Зарежда всички лица на даден потребител
    async getIndividuals ({ rootState }) {
      try {
        var data
        ({ data } = await axios.get('/individuals', { params: { _token: rootState.auth.token } }))

        data = _.map(data, e => {
          if (!e.picture) {
            e.picture = defAvatar
          }
          return e
        })

        rootState.user.individuals = data
        return data
      } catch ({ response }) {
        throw response
      };
    },

    // Зарежда лице с дадено id
    async getIndividual ({ rootState, state }, id) {
      try {
        var data
        ({ data } = await axios.get('/individual/' + id, { params: { _token: rootState.auth.token } }))

        if (!data.picture) {
          data.picture = defAvatar
        }

        state.individual = data
        return data
      } catch ({ response }) {
        throw response
      };
    }

  },
  mutations: {
    setIndividual (state, individual) {
      state.individual = individual
    }
  }
}
