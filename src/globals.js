import moment from 'moment'

let apiURL = process.env.VUE_APP__API_URL
let chatkitInstance = process.env.VUE_APP__CHATKIT_INSTANCE
let videoChatServiceUrl = process.env.VUE_APP__VIDEO_CHAT_URL

console.log(apiURL)

export default {
  apiURL,
  chatkitInstance,
  videoChatServiceUrl,
  pusher: {
    authUrl: apiURL + '/auth/pusher',
    appId: process.env.VUE_APP__PUSHER_ID,
    cluster: 'eu',
    forceTLS: true
  },
  skills: [
    'Първа помощ', 'Готвене', 'Чистене', 'Управление на МПС', 'Работа с деца', 'Компютърна грамотност'
  ],
  personTypes: [
    'Дете', 'Възрастен', 'Инвалид', 'Домашен любимец'
  ],
  daysOfTheWeek: [
    'ПОНЕДЕЛНИК',
    'ВТОРНИК',
    'СРЯДА',
    'ЧЕТВЪРТЪК',
    'ПЕТЪК',
    'СЪБОТА',
    'НЕДЕЛЯ'
  ],
  defIndividualAvatar: '/img/def_individual_avatar.png',
  daysOfTheWeekShort: [
    'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'НД'
  ],
  oauth: {
    google: {
      user: apiURL + '/' + 'oauth/google_user',
      caretaker: apiURL + '/' + 'oauth/google_caretaker'
    },
    facebook: {
      user: apiURL + '/' + 'oauth/facebook_user',
      caretaker: apiURL + '/' + 'oauth/facebook_caretaker'
    }
  },
  initialMsgIndicator: '#####!#####INITIAL_MESSAGE',
  age (d, number = false) {
    var y = moment(new Date()).diff(moment(d), 'years')
    if (y === 0) {
      var m = moment(new Date()).diff(moment(d), 'months')
      if (number) return m / 12
      return m + (m === 1 ? ' месец' : ' месеца')
    }
    if (number) return y
    return y + (y === 1 ? ' година' : ' години')
  },

  duration (start, end) {
    var sHours = start
    var sMinutes = 0
    var eHours = end
    var eMinutes = 0

    if (typeof start !== 'number') {
      let s = start.split(':')
      sHours = Number(s[0])
      sMinutes = Number(s[1])
    }

    if (typeof end !== 'number') {
      let e = end.split(':')
      eHours = Number(e[0])
      eMinutes = Number(e[1])
    }

    let d = moment
      .duration(eHours, 'hours')
      .add(eMinutes, 'minutes') // end duration
      .subtract(sHours, 'hours')
      .subtract(sMinutes, 'minutes') // start duration

    return d
  },

  timeFromNow (d) {
    let t = moment(d)

    // Ако съобщението е преди повече от ден
    if (t.diff(moment(), 'days') <= 1 || t.hour() > moment().hour()) {
      return t.fromNow()
    }

    var h = t.hour() < 10 ? '0' + t.hour() : t.hour()
    let m = t.minute() < 10 ? '0' + t.minute() : t.minute()

    return `${h}:${m}`
  },

  time (t) {
    if (typeof t === 'string') {
      return t
    }
    t = t.toFixed(2)
    let h = Math.floor(t)
    h = h < 10 ? (h = '0' + h) : h
    let m = t * 100
    m = Math.round(Number(String(m).slice(-2)) * 0.6)
    m = m < 10 ? (m = '0' + m) : m

    return `${h}:${m}`
  }
}
