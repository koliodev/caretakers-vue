import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import axios from 'axios'
import Cookie from 'js-cookie'
import vuetify from './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import globals from '@/globals.js'
import ImageUploader from 'vue-image-upload-resize'
import moment from 'moment'
import { delay } from 'lodash'

Vue.use(ImageUploader)
Vue.config.productionTip = false

// Настройки на moment.js
moment.locale('bg')

// Настройки на axios http библиотеката
axios.defaults.baseURL = globals.apiURL
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.transformRequest.unshift(function (data) {
  if (typeof data !== 'undefined') {
    data['_token'] = Cookie.get('jwt_auth_token')
  }

  return data
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

// Изписва garfild в конзолата
if (process.env.NODE_ENV === 'production') {
  let printCnsl = () => {
    console.clear()
    console.log('%c </Garfild> 🐱😼 ', `
      color:white;
      background-color:#FF5722; 
      font-weight:bold; 
      font-size:55pt;
      text-align:left;
      font-family:"Roboto", sans-serif;
      `)
    delay(printCnsl, 1000)
  }
  delay(printCnsl, 1000)
}
