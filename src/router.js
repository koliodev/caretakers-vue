import Vue from 'vue'
import Router from 'vue-router'
import store from './store.js'
import _ from 'lodash'

Vue.use(Router)

// Availble meta
// requiresAuth
// guest

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue'),
      meta: {
        requiresAuth: true
      }
    },
    // GUEST ROUTES
    {
      path: '/login/:provider?/:provider_id?',
      name: 'login',
      component: () => import('./views/Auth/Login.vue'),
      meta: {
        guest: true
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import('./views/Auth/Signup.vue'),
      meta: {
        guest: true
      }
    },
    {
      name: 'signupUser',
      path: '/signup/user/:email?/:provider?/:provider_id?',
      component: () => import('./views/Auth/UserSignup.vue'),
      meta: {
        guest: true
      }
    },
    {
      name: 'signupCaretaker',
      path: '/signup/caretaker/:email?/:provider?/:provider_id?',
      component: () => import('./views/Auth/CaretakerSignup.vue'),
      meta: {
        guest: true
      }
    },
    {
      path: '/new/individual',
      name: 'addIndividual',
      component: () => import('./views/Individuals/NewIndividual.vue'),
      meta: {
        requiresAuth: true,
        requiresUser: true,
        allowParentRoot: true
      }
    },
    {
      path: '/edit/individual/:id',
      name: 'editIndividual',
      component: () => import('./views/Individuals/EditIndividual.vue'),
      meta: {
        requiresAuth: true,
        requiresUser: true,
        allowParentRoot: true
      }
    },
    {
      path: '/individuals',
      name: 'allIndividuals',
      component: () => import('./views/Individuals/Individuals.vue'),
      meta: {
        requiresAuth: true,
        requiresUser: true

      }
    },
    {
      path: '/individual/:id',
      name: 'individual',
      component: () => import('./views/Individuals/Individual.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        allowPageTitle: true
      }
    },
    {
      path: '/find/caretaker/for/:id?',
      name: 'findCaretakerFor',
      component: () => import('./views/Individuals/FindCaretaker.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        allowPageTitle: true,
        requiresUser: true
      }
    },
    {
      path: '/found/caretakers/',
      name: 'suggestedCaretakers',
      component: () => import('./views/Individuals/SuggestedCaretakers.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        requiresUser: true,
        allowPageTitle: true
      }
    },
    {
      path: '/send/request/to/caretaker/:caretakerId/for/:individualId?',
      name: 'sendRequest',
      component: () => import('./views/Caretaker/SendRequest.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        requiresUser: true,
        allowPageTitle: true
      }
    },
    {
      path: '/requests',
      name: 'hireRequests',
      component: () => import('./views/HireRequests/Requests.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: false,
        allowPageTitle: true
      }
    },
    {
      path: '/request/:id',
      name: 'hireRequest',
      component: () => import('./views/HireRequests/Request.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        allowPageTitle: true
      }
    },
    {
      path: '/caretaker/:id',
      name: 'caretakerProfile',
      component: () => import('./views/Caretaker/CaretakerProfile.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        allowPageTitle: true
      }
    },
    {
      path: '/callendar',
      name: 'callendar',
      component: () => import('./views/Caretaker/Callendar.vue'),
      meta: {
        requiresAuth: true,
        requiresCaretaker: true,
        allowPageTitle: true
      }
    },
    {
      path: '/message/:hireRequest',
      name: 'conversation',
      component: () => import('./views/Messages/Conversation.vue'),
      meta: {
        requiresAuth: true,
        allowParentRoot: true,
        allowPageTitle: true
      }
    },
    {
      path: '/messages',
      name: 'conversations',
      component: () => import('./views/Messages/Conversations.vue'),
      meta: {
        requiresAuth: true,
        allowPageTitle: true
      }
    },
    {
      path: '/notifications',
      name: 'notifications',
      component: () => import('./views/Notifications/Notifications.vue'),
      meta: {
        requiresAuth: true,
        allowPageTitle: true
      }
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import('./views/Profile/Settings.vue'),
      meta: {
        requiresAuth: true,
        allowPageTitle: true
      }
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./views/Profile/Profile.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      component: () => import('./views/NotFound.vue')
    }

  ]
})

// Middware
router.beforeEach(async (to, from, next) => {
  var user = store.getters.user
  var loading = store.getters.siteLoading

  // Sync-ва loading и user с vuex sotre-a
  // Подобрява ux при зареждане на страницата
  var sleep = ms => {
    return new Promise(resolve => setTimeout(resolve, ms))
  }
  while (loading) {
    user = store.getters.user
    loading = store.getters.siteLoading
    await sleep(50)
  }

  if (user === false || loading) return

  let isAuth = !_.isEmpty(user)
  let userType = user.caretaker ? 'caretaker' : 'user'

  // Route-a изисква потребителят да е логнат
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!isAuth) {
      return next({ path: '/login' })
    }
  }

  // Route-a изисква потребителят да НЕ е логнат
  if (to.matched.some(record => record.meta.guest)) {
    if (isAuth) {
      return next({ path: '/', name: 'home' })
    }
  }

  // Route-a изисква потребителят да е нормален user
  if (to.matched.some(record => record.meta.requiresUser)) {
    if (userType === 'caretaker') {
      return next({ path: '/', name: 'home' })
    }
  }

  // Route-a изисква потребителят да е caretaker
  if (to.matched.some(record => record.meta.requiresCaretaker)) {
    if (userType === 'user') {
      return next({ path: '/', name: 'home' })
    }
  }

  return next()
})

export default router
