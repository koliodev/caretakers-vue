import Vue from 'vue'
import Vuex from 'vuex'
import auth from './store/auth.js'
import caretakers from './store/caretaker.js'
import individuals from './store/individuals.js'
import parentNavigation from './store/parentNavigation'
import hireRequests from './store/hireRequests'
import messages from './store/messages'
import notifications from './store/notifications'
import user from './store/user'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    siteLoading: true, // (влияе на поведението) прявят се някакви специфични заякви към базта
    pageLoader: false, // (влияе на САМО на изгледа) показване на кръгчето, което се върти (при някакви заявки или много redirect-и от страница в страница, които user-a не бива да вижда)
    user: false,
    snackbar: {
      active: false,
      text: '',
      color: '',
      btn: null
    }

  },
  getters: {
    user: state => state.user,
    siteLoading: state => state.siteLoading
  },
  modules: {
    auth,
    individuals,
    parentNavigation,
    caretakers,
    hireRequests,
    messages,
    notifications,
    user
  },
  mutations: {
    setSnackbar (state, s) {
      state.snackbar = s
    },
    unsetLoading (state) {
      state.siteLoading = false
    },
    showLoader (state) {
      state.pageLoader = true
    },
    hideLoader (state) {
      state.pageLoader = false
    }
  }
})
